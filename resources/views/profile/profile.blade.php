@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Profile @endsection

@section('content')
 <!-- Bnr Header -->
 <section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h3>WE ARE JL DialMed</h3>
        <h1>We care about your health</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Profile</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    @if($_GET['p'] == 'history')
    <section class="white-bg p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="margin-bottom-60" style="text-align: left">
          <h4>History</h4>
          <hr>
          <p>
            JL DIALMED TRADING CO. was formed in collaboration of individuals with a combined
            experience in the field of Renal Industry for more than 21 years. It started in early 1998 as
            Aquality Trading until such time it become JL Dialmed Trading Co. under the partnership of
            Jefferlyx O. Natividad and Lea P. Baliday. We have developed various innovations that are
            widely used in hemodialysis industry today with a reputation as one of the highly recommended
            supplier of water treatment system technology and medical supplies for dialysis.
          </p>
          <p>
            Part of our services that we provide is a comprehensive preventive maintenance support to
            clients. We also provide consultancy in planning and construction of new dialysis centers from
            designing through licensing. In addition, due to increase of demand we have decided to add
            more service that meet the demand of hospital. Among these services are repair, rehabilitation,
            restoration and maintenance of hospital supplies, machines and equipment.
          </p>
          <p>
            JL DIALMED TRADING COMPANY will continue to innovate in order to achieve the highest
            possible respect in the industry and aims to become the leader in the industry and a company of
            choice in providing quality products, reliability and safe Water Treatment System.
          </p>
          <p>
            JL DIALMED TRADING COMPANY is located at #14 Maria Theresa Ave. Don Jose Heights,
            Commonwealth Avenue Quezon City, Philippines.
          </p>
        </div>
      </div>
    </section>
    @endif
    
    @if($_GET['p'] == 'message')
    <section class="white-bg p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="margin-bottom-60" style="text-align: left">
          <h4>Message</h4>
          <hr>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egetvel lacus pretium rhoncus a quis nisly Ut vehicula gravida dui in pulvinar donec diam elit consequat eget augue vitae aliquet sollicitudin. 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egetvel lacus pretium rhoncus a quis nisly Ut vehicula gravida dui in pulvinar donec diam elit consequat eget augue vitae aliquet sollicitudin. 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egetvel lacus pretium rhoncus a quis nisly Ut vehicula gravida dui in pulvinar donec diam elit consequat eget augue vitae aliquet sollicitudin. 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egetvel lacus pretium rhoncus a quis nisly Ut vehicula gravida dui in pulvinar donec diam elit consequat eget augue vitae aliquet sollicitudin. 
          </p>
        </div>
      </div>
    </section>
    @endif
    
    @if($_GET['p'] == 'company_profile')
    <section class="white-bg p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="margin-bottom-60" style="text-align: left">
          <h4>Company Profile</h4>
          <hr>
          <p>
            <strong>JL DIALMED TRADING CO.</strong> was formed and manage by Mr. Jefferlyx O. Natividad who has wide knowledge, expertise and experience in the field of Renal Industry for more than a decade and still counting. It is under the partnership with Ms. Lea P. Baliday.  JL DIALMED TRADING CO. is located at #14 Maria Theresa Ave. Don Jose Heights, Commonwealth Avenue Quezon City, Philippines. 
          </p>
          <p>
            Through the years the company have developed various innovations that are widely used in hemodialysis industry today with a reputation as one of the highly recommended supplier of water treatment system technology and consumables supplies for dialysis.  
          </p>
          <p>
            Part of our services that we provide is a comprehensive preventive maintenance support to clients. Helping them facilitate and consultancy in planning and construction of new dialysis centers, from designing through licensing. In addition, we have decided to add more service to meet the demand of hospital. Among these services are repair, rehabilitation, restoration and maintenance of hospital supplies, machines and equipment.
          </p>
          <p>
            JL DIALMED TRADING COMPANY will continue to innovate in order to achieve the highest possible respect in the industry and aims to become the leader in the industry and a company of choice in providing quality and cost effecient products, reliability and safe Water Treatment System.
          </p>
          <p>
            The company have well trained technicians based in Metro Manila under the supervision of Mr. Jefferlyx O. Natividad. We are expanding by employing more technical staff in order to deliver the demand of services in the industry.  They completed various seminars in order to update their knowledge and equipped with new development in Water Treatment System.
          </p>
        </div>
      </div>
    </section>
    @endif
    
    @if($_GET['p'] == 'mission_vission')
    <section class="white-bg p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="margin-bottom-60" style="text-align: left">
          <h4>Mission</h4>
          <hr>
          <p>
           Our mission is to deliver our product and services for Hemodialysis center with quality, properly
handled and punctual to the best of our ability.
          </p>
        </div>

          <!-- Heading -->
        <div class="margin-bottom-60" style="text-align: left">
          <h4>Vision</h4>
          <hr>
          <p>
            To be on top provider of cost- efficient supplies and services for Hemodialysis industries.
          </p>
        </div>
      </div>
    </section>
    @endif

  </div>
@endsection