@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Dialysis Equipment @endsection

@section('content')
<!-- Bnr Header -->
<section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>DIALYSIS EQUIPMENT</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">DIALYSIS EQUIPMENT</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
<div id="content"> 
    
    <section class="white-bg p-t-b-150">
        <div class="container"> 
            
        <div class="margin-bottom-60" style="text-align: left">
          <h4>Under Construction</h4>
          <hr>
          <p>Content Under construction</p>
        </div>
      </div>
        </section>
</div>
@endsection