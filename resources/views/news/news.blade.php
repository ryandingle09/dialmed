@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - News @endsection

@section('content')
<!-- Bnr Header -->
<section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h3>WE ARE JL DialMed</h3>
        <h1>NEWS</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">NEWS</li>
        </ol>
      </div>
    </div>
</section>
  
  <!-- Content -->
<div id="content"> 
    <!-- Search -->
    <div class="container side-bar" style="margin-top: 2%">
        <form method="GET" action="{{ route('news') }}">
            <div class="search col-md-4 pull-right">
                <div class="form-group">
                <input name="search" id="serch" type="search" class="input font-crimson" placeholder="Search.." value="{{ (isset($_GET['search'])) ? $_GET['search']: '' }}">
                <button class="sea-icon"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
<!-- News -->
    <section class="p-t-b-150">
        <div class="container"> 
            <!-- Latest News -->
            <div class="news">
                <div class="row"> 
                    @if(count($news) == 0)
                    
                    @if(isset($_GET['search']))
                        <h3>No results found for "{{ $_GET['search'] }}"</h3>
                    @elseif(isset($_GET['tag']))
                        <h3>No results found for this tag.</h3>
                    @elseif(isset($_GET['category']))
                        <h3>No results found for this category.</h3>
                    @else   
                        <h3>No news article yet!</h3>
                    @endif

                    @else

                        @foreach($news as $n)
                            @php
                                if(isset($_GET['tag'])) $n = $n->news
                            @endphp
                            <div class="col-md-4"> 
                                <article class="news-post">
                                <div class="post-img"> <img class="img-responsive" src="{{ $n->cover_image }}" alt="">
                                    <div class="date"> {{ date('d M', strtotime($n->created_at)) }} </div>
                                </div>
                                <a href="{{ route('news.show', [$n->slug]) }}" class="tittle">{{ Str::limit($n->title, 33) }}</a> <span class="by">By {!! $n->author->name !!}</span>
                                <p>
                                    {{ strip_tags(Str::limit($n->body, 500)) }}
                                </p>
                                
                                <div class="post-info"> 
                                    {{-- <span><i class="fa fa-comments-o margin-right-10"></i>97</span> 
                                    <span><i class="fa fa-eye margin-right-10"></i>563</span>  --}}
                                    <span><i class="fa fa-tag margin-right-10"></i>{{ $n->category['title'] }}</span> 
                                    <a href="{{ route('news.show', [$n->slug]) }}">More <i class="fa fa-angle-right margin-left-10"></i></a> 
                                </div>
                                </article>
                            </div>
                        @endforeach



                        {{ $news->links() }}

                    @endif
                </div>
            </div>

        </div>

    </section>
</div>
@endsection