@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - News Post @endsection

@section('content')
  <!-- Bnr Header -->
  <section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h3>WE ARE JL DialMed</h3>
        <h1>NEWS</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">NEWS</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- News Posts -->
    <section class="blog blog-single padding-top-80 padding-bottom-80">
      <div class="container">
        <div class="row"> 
          
          <!-- Posts -->
          <div class="col-md-9 padding-right-50">
            <article class="blog-posts"> <img class="img-responsive" src="{{ $news->cover_image }}" alt="" > 
              <!-- Tittle -->
              <h4>{{ $news->title }}</h4>
              <div class="post-info">
                <span><i class="fa fa-user margin-right-10"></i>{!! $news->author->name !!}</span> 
                {{-- <span><i class="fa fa-comments-o margin-right-10"></i>97</span>  --}}
                {{-- <span><i class="fa fa-eye margin-right-10"></i>563</span>  --}}
                <span><i class="fa fa-tag margin-right-10"></i>{{ $news->category->title }}</span> 
              </div>

              {!! $news->body !!}

              <!-- Bnt Info -->
              {{-- <h6>Share</h6>
              <div class="social-icons share"> 
                <a href="#."><i class="fa fa-facebook"></i></a> 
                <a href="#."><i class="fa fa-twitter"></i></a> 
                <a href="#."><i class="fa fa-google-plus"></i></a> 
                <a href="#."><i class="fa fa-pinterest-p"></i></a> 
              </div> --}}
            </article>
            <div class="comments"> 
              
              <!-- Main Heading -->
              <div class="heading-side-bar margin-bottom-40 margin-top-80">
                <h4>Comment (37)</h4>
              </div>
              <ul>
                
                <!-- Comments -->
                <li class="margin-bottom-30">
                  <div class="media">
                    <div class="media-left">
                      <div class="avatar"><img class="img-responsive" src="/public/images/doc-img-1.jpg" alt=""></div>
                    </div>
                    <div class="media-body">
                      <div class="a-com"> <span class="a-name">JAMMIE LANDING </span><span class="date">24.03.2015 at 10:21</span>
                        <p class="margin-top-20">“Quando feugait duo ei, te erant corpora interpretaris eos. Illud accommodare vituperatoribus an mea. Erat mazim animal 
                          at nam, eam te doctus evertitur, sed decore ornatus”</p>
                        <a href="#." class="text-right"> REPLAY </a> </div>
                    </div>
                  </div>
                </li>
                
                <!-- Comments Replay -->
                <li class="com-reply">
                  <div class="media">
                    <div class="media-left">
                      <div class="avatar"><img class="img-responsive" src="/public/images/doc-img-2.jpg" alt=""></div>
                    </div>
                    <div class="media-body">
                      <div class="a-com"> <span class="a-name">JAMMIE LANDING </span><span class="date">24.03.2015 at 10:21</span>
                        <p class="margin-top-20">“Quando feugait duo ei, te erant corpora interpretaris eos. Illud accommodare vituperatoribus an mea. Erat mazim animal 
                          at nam, eam te doctus evertitur, sed decore ornatus”</p>
                        <a href="#." class="text-right"> REPLAY </a> </div>
                    </div>
                  </div>
                </li>
              </ul>
              
              <!-- Comments Form -->
              <div class="comment-form"> 
                <!-- Main Heading -->
                <div class="heading-side-bar margin-bottom-40 margin-top-50">
                  <h4>REPLY COMMENT</h4>
                </div>
                <form>
                  <ul class="row">
                    <li class="col-sm-4">
                      <label>*NAME
                        <input class="form-control" type="text" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-4">
                      <label>*EMAIL ADDRESS
                        <input class="form-control" type="text" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-4">
                      <label> WEBSITE
                        <input class="form-control" type="text" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-12">
                      <label> SUBJECT
                        <input class="form-control" type="text" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-12">
                      <textarea placeholder="YOUR MESSAGE"></textarea>
                    </li>
                    <li class="col-sm-12">
                      <button type="submit" class="btn btn-dark">POST COMMENT </button>
                    </li>
                  </ul>
                </form>
              </div>
            </div>
          </div>
          
          <!-- Side Bars -->
          <div class="col-md-3">
            <div class="side-bar"> 
              
              <!-- Search -->
              <form method="GET" action="{{ route('news') }}">
              <div class="search">
                <div class="form-group">
                  <input name="search" id="serch" type="search" class="input font-crimson" placeholder="Search..">
                  <button class="sea-icon"><i class="fa fa-search"></i></button>
                </div>
              </div>
              </form>
              
              <!-- About Me -->
              <div class="about-me-small text-center margin-top-40"> <img src="/public/images/depart-doctor-img.jpg" alt="">
                <h6 class="margin-top-20 margin-bottom-1">WHITE SHADOW WALKER</h6>
                <span class="font-12px">Writer/Personal blog</span> </div>
              
              <!-- Main Heading -->
              <div class="heading-side-bar margin-bottom-20">
                <h4>Latest Articles</h4>
              </div>
              <ul class="latest-post">
                @if(count($recent_news) == 0)
                  <h5>No recent news yet!</h5>
                @else
                  @foreach($recent_news as $rs)
                    <li>
                        <div class="media">
                          <div class="media-left"> <a href="{{ route('news.show', [$rs->slug]) }}"><img src="{{ $rs->cover_image }}" alt=""></a></div>
                          <div class="media-body"> <a href="{{ route('news.show', [$rs->slug]) }}">{{ Str::limit($rs->title,30) }}</a>
                            <p class="font-12px font-italic">{{ $rs->category->title }}</p>
                          </div>
                        </div>
                    </li>
                  @endforeach
                @endif
              </ul>
              
              <!-- Main Heading -->
              <div class="heading-side-bar margin-bottom-20">
                <h4>Categories</h4>
              </div>
              <ul class="cate">
                @if(count($recent_news) == 0)
                  <h5>No categories yet!</h5>
                @else
                  @foreach($categories as $c)
                  <li><a href="{{ route('news.category.list') }}?category={{ $c->id }}">{{ $c->title }}</a></li>
                  @endforeach
                @endif
              </ul>
              
              <!-- Main Heading -->
              <div class="heading-side-bar margin-bottom-40">
                <h4>TAGS CLOUD</h4>
              </div>
              <ul class="tags">
                @if(count($recent_news) == 0)
                  <h5>No recent tags yet!</h5>
                @else
                  @foreach($tags as $t)
                  <li><a href="{{ route('news.tag.list') }}?tag={{ $t->id }}">{{ $t->title }}</a></li>
                  @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection