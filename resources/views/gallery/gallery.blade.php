@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Gallery @endsection

@section('content')
<!-- Bnr Header -->
<section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h3>WE ARE JL DialMed</h3>
        <h1>GALLERY</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">GALLERY</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- PORTFOLIO -->
    <section class="portfolio port-wrap p-t-b-150"> 
      @if(count($gallery) == 0)
      <h4 class="text-center">No Photos Yet!</h4>
      @else
      <ul class="portfolio-filter text-center margin-bottom-50">
        <li><a class="active" href="#." data-filter="*">AlL </a></li>
        @foreach($gallery as $g)
            <li><a href="#." data-filter=".{{$g->id}}"> {{$g->folder}} </a></li>
        @endforeach
      </ul>
      
      <!-- PORTFOLIO ITEMS -->
      <div class="container">
        <div class="items item-space row col-3 popup-gallery"> 
          
          @foreach($images as $i)

          <article class="portfolio-item {{ $i->gallery }}">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="{{ $i->image_full_path }}">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    @php
                      $gallery_title = '';
                    
                      foreach($gallery as $g):
                        if($g->id == $i->gallery) $gallery_title = $g->folder;
                      endforeach;
                    @endphp
                    <h3>{{ $gallery_title }}</h3>
                    <hr class="balck">
                    <a href="{{ $i->image_full_path }}" title="{{ $gallery_title }}"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>

          @endforeach

        </div>

        {{$images->links()}}

        @endif
      </div>
    </section>
  </div>
@endsection