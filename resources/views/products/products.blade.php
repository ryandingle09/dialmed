@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Renal Products @endsection

@section('content')
<!-- Bnr Header -->
<section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>RENAL PRODUCTS</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">RENAL PRODUCTS</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 

    <section class="p-t-b-150">

        <div class="container">

            <div class="intro-main">

                @if($_GET['rp'] == '1')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diacid® Acid Hemodialysis Concentrates  (36.83X)(35X)(45X)</h3>
                            <small><b>Product Number:  HX-AFf2211</b></small>
                            <p>our Manufacturer uses AAMI quality water to produce non-pyrogenic Hemodialysis concentrate. All raw materials used for manufacturing Hemodialysis concentrates are pharmaceutical Grade quality or higher. Final product is meticulously checked by our Quality Control laboratory using the latest analytical equipment to exact specifications. All of our liquid & Powder concentrate are thoroughly blended, filtered and tested throughout the manufacturing process.</p>
                            <hr />

                            <h4>Features</h4>
                            
                            <ul class="row">
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Designed for use with (36.83x)(35X)(45X) hemodialysis machines</p>
                            </li>
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Diaced® Acid Concentrate available in both liquid and powder</p>
                            </li>
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Diaced® Acid concentrate sold by the case and jug for most formulations</p>
                            </li>
                            </ul>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                            <img class="img-responsive intro-img" src="/public/images/products/IMG_3.jpg" > 
                        <div class="col-md-12">
                            <br/><br/>
                            <section class="young-doc-team ">
                                <div class="doct-list">
                                    <div class="item"> <a href="#p1" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_1.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p1" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_2.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p3" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_3.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p4" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_4.jpg" alt="" ></a> </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <hr />

                <div class="row p-t-b-150"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diacarb® Bicarb Hemodialysis Concentrate (36.83X)(35X)(45X)</h3>
                            <small><b>Product Number:  HX-BFf</b></small>
                            <p>
                            The Diaced® acid concentrate series is formulated to be mixed in conjunction with the Diacarb® Sodium Bicarbonate Concentrate or equivalent in a compatible (36.83X)(35X)(45X) dilution three-stream artificial kidney (hemodialysis) machine.  Refer to instructions provided by artificial kidney machine manufacturer prior to starting dialysis.  (AAMI quality).
                            </p>
                            <hr />

                            <h4>Features</h4>
                            
                            <ul class="row">
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Designed for use with (36.83x)(35X)(45X) hemodialysis machines</p>
                            </li>
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Diacarb® Acid Concentrate available in both liquid and powder</p>
                            </li>
                            <li class="col-sm-6">
                                <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                <p>Diacarb® Bicarb Concentrate sold by the case and jug for most formulations</p>
                            </li>
                            </ul>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/IMG_1.jpg" > 
                        <div class="col-md-12">
                            <br/><br/>
                            <section class="young-doc-team ">
                                <div class="doct-list">
                                    <div class="item"> <a href="#p21" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_21.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p21" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_22.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p23" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_23.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p24" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_24.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p25" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_25.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p26" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_26.jpg" alt="" ></a> </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <div id="p1" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_1.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p2" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_2.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p3" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_3.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p4" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_4.jpg" alt="" > </div>
                    </div>
                </div>

                <div id="p21" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_21.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p22" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_22.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p23" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_23.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p24" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_24.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p25" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_25.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p26" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_26.jpg" alt="" > </div>
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '2')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>HemoDialysis Blood Tubing Set  3 + 1</h3>
                            <small><b></b></small>
                            <p>
                            Hemodialysis Bloodlines Set consist of Arterial and venous lines, used during dialysis and are attached with Arterial / Venous Fistula Set and Dialyzer.
                            </p>
                            <p>
                            E.O. Gas Sterilization     
                            </p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/bloodlines.jpg" > 
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '3')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Arterial / Venous Fistula Set</h3>
                            <small><b></b></small>
                            <p>
                            Arterial and Venous Set for Hemodialysis ( Fixed Wing or Rotating )<br>
                            Gauge 15, 16, 17  ( With Back eye needle )
                            </p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/arterial.jpg" > 
                    </div>
                </div>
                
                @endif

                @if($_GET['rp'] == '4')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Heparine Sodium</h3>
                            <small><b></b></small>
                            <p>Heparin Sodium Injection, USP is a sterile, nonpyrogenic solution of heparin sodium (derived from porcine intestinal mucosa) in water for injection. Each container contains 10000, 12500, 20000 or 25,000 USP Heparin Units; 40 or 80 mg sodium chloride added to render isotonic (see HOW SUPPLIED section for various sizes and strength). May contain sodium hydroxide and/or hydrochloric acid for pH adjustment. pH 6.0 (5.0 to 7.5).

The solution contains no bacteriostat, antimicrobial agent or added buffer and is intended for use only as a single-dose injection. When smaller doses are required, the unused portion should be discarded.

Heparin sodium in the ADD-Vantage™ system is intended for intravenous administration only after dilution.

Heparin Sodium, USP is a heterogenous group of straight-chain anionic mucopolysaccharides, called glycosamino-glycans having anticoagulant properties. Although others may be present, the main sugars occurring in heparin are: (1) α- L-iduronic acid 2-sulfate, (2) 2-deoxy-2-sulfamino-α-D-glucose-6-sulfate, (3) β-D-glucuronic acid, (4) 2-acetamido-2-deoxy-α-D-glucose, and (5) α-L-iduronic acid. These sugars are present in decreasing amounts, usually in the order (2) > (1) > (4) > (3) > (5), and are joined by glycosidic linkages, forming polymers of varying sizes. Heparin is strongly acidic because of its content of covalently linked sulfate and carboxylic acid groups. In heparin sodium, the acidic protons of the sulfate units are partially replaced by sodium ions. The potency is determined by a biological assay using a USP reference standard based on units of heparin activity per milligram.</p>
                            <hr />

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/arterialvenous.jpg" > 
                        <div class="col-md-12">
                            <br/><br/>
                            <section class="young-doc-team ">
                                <div class="doct-list">
                                    <div class="item"> <a href="#p1" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0474.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p2" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0476.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p3" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0480.jpg" alt="" ></a> </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <div id="p1" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0474.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p2" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0476.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p3" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0480.jpg" alt="" > </div>
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '5')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diakit Fistula Kit</h3>
                            <small><b></b></small>
                            <p>No Description</p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/fistula.jpg" > 
                    </div>
                </div>

                <hr />

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diakit Subclavian Kit</h3>
                            <small><b></b></small>
                            <p>No Description</p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/subclavian.jpg" > 
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '6')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diaclean®  Peracetic Acid Test Strips</h3>
                            <small><b></b></small>
                            <p>
                            A dip and read test strip that indicates a minimum level of peracetic acid at 500 ppm after storage and prior to rinsing for clinical use.
                            100 Strips per Bottle.
                            </p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/peracetic.jpg" > 
                    </div>
                </div>

                <hr />

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diaclean® Residual Test Strips</h3>
                            <small><b></b></small>
                            <p>
                            Tests for a quantitative measure Diaclean® concentration after rinse-out. This test in sensitive to one part per million.
                            100 per Bottle.
                            </p>

                            <hr />

                            <h4>Features</h4>
                            
                            <ul class="row">
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                    <p>5 second read time</p>
                                </li>
                            </ul>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/residual.jpg" > 
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '7')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Hemodialysis Dialyzer High/Low Flux Dialyzer</h3>
                            <small><b></b></small>
                            <p>No Description</p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/IMG_0527.jpg" > 
                        <div class="col-md-12">
                            <br/><br/>
                            <section class="young-doc-team ">
                                <div class="doct-list">
                                    <div class="item"> <a href="#p1" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0527.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p2" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0515.jpg" alt="" ></a> </div>
                                    <div class="item"> <a href="#p3" class="link popup-vedio video-btn"><img class="img-responsive" src="/public/images/products/IMG_0529.jpg" alt="" ></a> </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <div id="p1" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0527.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p2" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0515.jpg" alt="" > </div>
                    </div>
                </div>
                <div id="p3" class="zoom-anim-dialog mfp-hide pop-open-style">
                    <div class="pop_up row"> 
                        <div class="col-sm-12 no-padding-left"> <img class="img-responsive" src="/public/images/products/IMG_0529.jpg" alt="" > </div>
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '8')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Citroclean® Heat Disinfection</h3>
                            <small><b></b></small>
                            <p>For Heat Disinfection of hemodialysis machine.</p>

                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/IMG_0007.jpg" > 
                    </div>
                </div>

                @endif

                @if($_GET['rp'] == '9')

                <div class="row"> 
                    
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h3>Diaclean® Cold Sterilant</h3>
                            <small><b></b></small>
                            <p>
                            Diaclean® Cold Sterilant is the packaging solution for increasing your reprocessing efficiency. Diaclean® Cold Sterilant offers the same proven performance and cleaning and sterilization power of traditional Diaclean® Cold Sterilant in a convenient package that requires no manual dilution and uses minimal storage space.
                            </p>

                            <hr />

                            <h4>Features</h4>
                            
                            <ul class="row">
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                    <p>
                                    Effective for disinfecting dialyzer, Haemodialysis machine and Water Treatment system. Each container holds Five liters of Diaclean® Cold Sterilant. To prevent excessive pressure build-up, all concentrate containers are provided with vented caps. These caps should not be altered or replaced. All boxes must be stored in an upright position. Diaclean® Cold Sterilant comes with four containers per case and can be ordered with or without test strips-kitted product.
                                    </p>
                                </li>
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> </h6>
                                    <p>
                                    Flexible : Most dialysis centers order two sizes of Diaclean® Cold Sterilant; one for dialyzer reprocessing and another to use as a disinfectant for ancillary supplies at a 1% concentration. Diaclean® Cold Sterilant convenient packaging offers a flexible solution for both dialyzer reprocessing and ancillary supply disinfection.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <!-- Intro Timing -->
                    <div class="col-md-5"> 
                        <img class="img-responsive intro-img" src="/public/images/products/IMG_0044.jpg" > 
                    </div>
                </div>

                @endif
            </div>
        </div>

    </section>

</div>
@endsection