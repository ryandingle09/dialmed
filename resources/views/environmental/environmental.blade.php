@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Environmental @endsection

@section('content')
<!-- Bnr Header -->
<section class="sub-bnr" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>ENVIRONMENTAL</h1>
        
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">ENVIRONMENTAL</li>
        </ol>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- PORTFOLIO -->
    <section class="container port-wrap p-t-b-150"> 
      
      <p>Soon Content here</p>

    </section>
  </div>
@endsection