<?php 
$route = Route::currentRouteName();
$list = 'verification password register login home about services contact gallery blog news.show news profile equipment products environmental';
//if(strpos($list, $route) !== false):
?>

<div class="top-bar">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <p>Welcome To JL Dialmed Trading Company</p>
      </div>
      <div class="col-sm-3">
        <!-- <div class="social-icons"> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-twitter"></i></a> <a href="#."><i class="fa fa-dribbble"></i></a> <a href="#."><i class="fa fa-instagram"></i></a> </div> -->
        <div class="form-input">
          <input type="text" placeholder="Search" class="form-control" focus="off" style="border-radius: 0px;margin-top: 0.9%">
        </div>
      </div>
    </div>
  </div>
</div>
<header id="header">
  <div class="container">
    <div class="logo"> 
      <a href="/">
        <img src="/public/images/header_logo.png" alt="JLDialMed" style="width: 150px; height: 40px" />
      </a> 
    </div>
    
    <nav class="navbar ownmenu">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon"></i></span> </button>
      </div>
      <div class="collapse navbar-collapse" id="nav-open-btn">
        <ul class="nav">
          <li><a href="{{ route('home') }}">Home</a></li>
          <li class="@if($route = Route::currentRouteName() == 'profile') active @endif dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile</a>
            <ul class="dropdown-menu animated-2s">
              <li class="@if(isset($_GET['p']) == true && $_GET['p'] == 'history') active @endif"><a href="{{ route('profile') }}?p=history">History</a></li>
              <li class="@if(isset($_GET['p']) == true && $_GET['p'] == 'message') active @endif"><a href="{{ route('profile') }}?p=message">Message</a></li>
              <li class="@if(isset($_GET['p']) == true && $_GET['p'] == 'company_profile') active @endif"><a href="{{ route('profile') }}?p=company_profile">Company Profile</a></li>
              <li class="@if(isset($_GET['p']) == true && $_GET['p'] == 'mission_vission') active @endif"><a href="{{ route('profile') }}?p=mission_vission">Mission/Vision</a></li>
            </ul>
          </li>
          <li @if($route = Route::currentRouteName() == 'news') class="active" @endif><a href="{{ route('news') }}">News </a></li>
          <li @if($route = Route::currentRouteName() == 'gallery') class="active" @endif><a href="{{ route('gallery') }}">Gallery</a></li>
          <li class="@if($route = Route::currentRouteName() == 'products') active @endif dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Renal Products</a>
            <ul class="dropdown-menu animated-2s">
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '1') active @endif"><a href="{{ route('products') }}?rp=1">Hemodialysis Concentrate</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '2') active @endif"><a href="{{ route('products') }}?rp=2">Bloodlines</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '3') active @endif"><a href="{{ route('products') }}?rp=3">Arterial / VenousFistula Fistula</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '4') active @endif"><a href="{{ route('products') }}?rp=4">Heparin</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '5') active @endif"><a href="{{ route('products') }}?rp=5">Kits</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '6') active @endif"><a href="{{ route('products') }}?rp=6">Test Strips</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '7') active @endif"><a href="{{ route('products') }}?rp=7">Dialyzer</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '8') active @endif"><a href="{{ route('products') }}?rp=8">Citroclean®</a></li>
              <li class="@if(isset($_GET['rp']) == true && $_GET['rp'] == '9') active @endif"><a href="{{ route('products') }}?rp=9">Diaclean®</a></li>
            </ul>
          </li>
          <li class="@if($route = Route::currentRouteName() == 'equipment') active @endif dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Dialysis Equipment</a>
            <ul class="dropdown-menu animated-2s">
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '1') active @endif"><a href="{{ route('equipment') }}?e=1">Water System</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '2') active @endif"><a href="{{ route('equipment') }}?e=2">Reprocessing Machine</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '3') active @endif"><a href="{{ route('equipment') }}?e=3">Mixer</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '4') active @endif"><a href="{{ route('equipment') }}?e=4">Scale</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '5') active @endif"><a href="{{ route('equipment') }}?e=5">Chair</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '6') active @endif"><a href="{{ route('equipment') }}?e=6">Dialyzer Rack</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '7') active @endif"><a href="{{ route('equipment') }}?e=7">Renovation</a></li>
              <li class="@if(isset($_GET['e']) == true && $_GET['e'] == '8') active @endif"><a href="{{ route('equipment') }}?e=8">STP</a></li>
            </ul>
          </li>
          <li @if($route = Route::currentRouteName() == 'environmental') class="active" @endif><a href="{{ route('environmental') }}">Environmental</a></li>
          <li @if($route = Route::currentRouteName() == 'contact') class="active" @endif><a href="{{ route('contact') }}">Contact</a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<?php //else:?>

{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
      <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">

          </ul>

          <ul class="navbar-nav ml-auto">
              @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav> --}}

<?php //endif;?>