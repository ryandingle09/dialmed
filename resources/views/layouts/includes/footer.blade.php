<!-- Footer -->
<footer class="footer-2">
    <div class="container">
      <div class="row"> 
        
        <!-- Support -->
        <div class="col-sm-5">
          <h6>About Us</h6>
          <div class="about-foot">
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egetvel lacus pretium rhoncus a quis nisly Ut vehicula gravida dui in pulvinar donec diam elit consequat eget augue vitae aliquet sollicitudin. </p>
          </div>
        </div>
        
        <!-- Latest News News -->
        <div class="col-md-3">
          <h6>Latest News Posts</h6>

          <div class="latest-post-small"> 
          
          @foreach(\App\News::take(3)->get() as $n):

            <div class="media">
              <div class="media-left"> <a href="{{ route('news.show', [$n->slug]) }}" class="post-img"><img src="{{ $n->cover_image }}" alt="{{ $n->title }}" class="img-responsive"></a> </div>
              <div class="media-body"> <a href="{{ route('news.show', [$n->slug]) }}">{{ Str::limit($n->title,30) }}</a> <span>{{ date('d/m/Y', strtotime($n->created_at)) }}</span> </div>
            </div>

            @endforeach

          </div>
        </div>
        
        <!-- Conatct -->
        <div class="col-sm-3">
          <div class="con-info">
            <h6>Contact Us</h6>
            <p>
              JL Dialmed Trading Company
              14 Maria Theresa St., 
              Don Jose Heights, Commonwealth, Quezon City, Philippines, 1121
              </p>
            <ul>
              <li class="mobile">
                <p>Telephone: (0632) 7799-2665</p>
              </li>
              <li class="email">
                <p>Email: sales@jldialmed.com</p>
                <p>Website: www.jldialmed.com</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <!-- Rights -->
  <div class="rights style-2">
    <div class="container">
      <p>© <?php echo date('Y'); ?> JLDialMed. Made by webpremiere.ph</p>
    </div>
  </div>
  
  <!-- GO TO TOP  --> 
  <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP  --> 