@extends('layouts.app')

@section('title') {{ config('app.name', 'Laravel') }} - Home @endsection

@section('content')
<!-- Bnr Header -->
<!-- <section class="main-banner">
  <div class="tp-banner-container">
    <div class="tp-banner">
      <ul>
        
        <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >  
          <img src="/public/images/slider-bg-1.jpg"  alt="slider"  data-bgposition=" center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
          
          <div class="tp-caption sfl tp-resizeme" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="-120" 
              data-speed="800" 
              data-start="800" 
              data-easing="Power3.easeInOut" 
              data-splitin="chars" 
              data-splitout="none" 
              data-elementdelay="0.03" 
              data-endelementdelay="0.4" 
              data-endspeed="300"
              style="z-index: 5; font-size:50px; font-weight:500; color:#fff;  max-width: auto; max-height: auto; white-space: nowrap;">Hello, We are DialMed!!</div>
          
          <div class="tp-caption sfr tp-resizeme" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="-60" 
              data-speed="800" 
              data-start="1000" 
              data-easing="Power3.easeInOut" 
              data-splitin="chars" 
              data-splitout="none" 
              data-elementdelay="0.03" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              style="z-index: 6; font-size:40px; color:#fff; font-weight:500; white-space: nowrap;">We care about your health </div>
          
          <div class="tp-caption sfb tp-resizeme" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="0" 
              data-speed="800" 
              data-start="1200" 
              data-easing="Power3.easeInOut" 
              data-splitin="none" 
              data-splitout="none" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              style="z-index: 7;  font-size:22px; color:#fff; font-weight:500; max-width: auto; max-height: auto; white-space: nowrap;">Best Hospitality Services in your town</div>
          
          <div class="tp-caption lfb tp-resizeme scroll" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="120"
              data-speed="800" 
              data-start="1300"
              data-easing="Power3.easeInOut" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              data-scrolloffset="0"
              style="z-index: 8;"><a href="{{ route('gallery') }}" class="btn">VIEW SERVICES</a> </div>
        </li>
        
        <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
          <img src="/public/images/slider-bg-2.jpg"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
          
          <div class="tp-caption sfl tp-resizeme" 
              data-x="left" data-hoffset="0" 
              data-y="center" data-voffset="-100" 
              data-speed="800" 
              data-start="800" 
              data-easing="Power3.easeInOut" 
              data-splitin="chars" 
              data-splitout="none" 
              data-elementdelay="0.03" 
              data-endelementdelay="0.4" 
              data-endspeed="300"
              style="z-index: 5; font-size:40px; font-weight:500; color:#000;  max-width: auto; max-height: auto; white-space: nowrap;">Best Services </div>
          
          <div class="tp-caption sfr tp-resizeme" 
              data-x="left" data-hoffset="0" 
              data-y="center" data-voffset="-40" 
              data-speed="800" 
              data-start="800" 
              data-easing="Power3.easeInOut" 
              data-splitin="chars" 
              data-splitout="none" 
              data-elementdelay="0.03" 
              data-endelementdelay="0.4" 
              data-endspeed="300"
              style="z-index: 5; font-size:55px; font-weight:500; color:#000;  max-width: auto; max-height: auto; white-space: nowrap;">Hello, We are DialMed!!</div>
          
          <div class="tp-caption sfb tp-resizeme" 
              data-x="left" data-hoffset="0" 
              data-y="center" data-voffset="30" 
              data-speed="800" 
              data-start="1400" 
              data-easing="Power3.easeInOut" 
              data-splitin="none" 
              data-splitout="none" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              style="z-index: 7; font-size:16px; color:#000; font-weight:500; line-height:26px; max-width: auto; max-height: auto; white-space: nowrap;">It has survived not only five centuries, but also the leap into
            electronic <br>
            typesetting, remaining essentially unchanged. </div>
          
          <div class="tp-caption lfb tp-resizeme scroll" 
              data-x="left" data-hoffset="0" 
              data-y="center" data-voffset="140"
              data-speed="800" 
              data-start="1600"
              data-easing="Power3.easeInOut" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              data-scrolloffset="0"
              style="z-index: 8;"><a href="{{ route('contact') }}" class="btn">CONTACT NOW</a> </div>
        </li>
        
        <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
          <img src="/public/images/slider-bg-3.jpg"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
          
          <div class="tp-caption sfb tp-resizeme" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="-80" 
              data-speed="800" 
              data-start="800" 
              data-easing="Power3.easeInOut" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              data-scrolloffset="0"
              style="z-index: 6; font-size:40px; color:#000; font-weight:500; white-space: nowrap;"> Welcome To Our Product Name </div>
          
          <div class="tp-caption sfb tp-resizeme text-center" 
              data-x="center" data-hoffset="0" 
              data-y="center" data-voffset="-10" 
              data-speed="800" 
              data-start="1000" 
              data-easing="Power3.easeInOut" 
              data-elementdelay="0.1" 
              data-endelementdelay="0.1" 
              data-endspeed="300" 
              data-scrolloffset="0"
              style="z-index: 7; font-size:20px; font-weight:500; line-height:26px; color:#000; max-width: auto; max-height: auto; white-space: nowrap;">We work in a friendly and efficient using the latest <br>
            technologies and sharing our expertise.</div>
        </li>
      </ul>
    </div>
  </div>
</section> -->

<section class="testimonial p-t-b-150" style="height: 600px" style="background: url(/public/images/top-bg.jpg) center center no-repeat;background-size: cover;">
  <div class="container"> 
    
    <div class="heading-block margin-bottom-60">
      <h2>Welcome to JL Dialmed Company</h2>
      <hr>
    </div>

  </div>
</section>

<!-- Content -->
<div id="content"> 
    
    <!-- Intro -->
    <section class="p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="heading-block">
          <h2>Our Services</h2>
          <hr>
          <span>Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie conseu vel illum dolore eufe ugiat nulla facilisis at vero.</span> </div>
        
        <!-- Services -->
        <div class="services">
          <div class="row"> 
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-eye-2 icon"></i> </div>
                <div class="media-body">
                  <h6>Eye Specialist</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-operating-room icon"></i> </div>
                <div class="media-body">
                  <h6>Operation Theater</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-icu-monitor icon"></i> </div>
                <div class="media-body">
                  <h6>ICU Department</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-doctor icon"></i> </div>
                <div class="media-body">
                  <h6>Qualified Doctors</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-heartbeat icon"></i> </div>
                <div class="media-body">
                  <h6>Heart Problems</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-stomach-2 icon"></i> </div>
                <div class="media-body">
                  <h6>Stomach Problems</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </section>

    <hr />

    <section class="portfolio port-wrap" style="margin-top: 80px;"> 

      <div class="heading-block">
            <h2>Products and Equipment</h2>
      </div>

      <div class="container">
        <div class="items item-space row col-3 popup-gallery"> 
          
          <!-- ITEM -->
          <article class="portfolio-item treat lab event">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-1.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-1.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item doc treat  lab event">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-2.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-2.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item doc treat pait sur lab ">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-3.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-3.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item  pait pait lab event">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-4.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-4.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item treat pait pait sur ">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-5.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-5.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item doc pait sur ">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-6.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-6.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item doc pait pait  event">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-7.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-7.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item pait pait sur lab">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-8.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-8.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
          
          <!-- ITEM -->
          <article class="portfolio-item doc treat sur lab">
            <div class="portfolio-image"> <img class="img-responsive" alt="Open Imagination" src="/public/images/img-9.jpg">
              <div class="portfolio-overlay">
                <div class="detail-info">
                  <div class="position-center-center">
                    <h3>Product Name</h3>
                    <hr class="balck">
                    <span>JL DialMed, Products</span> <a href="images/img-9.jpg" title="Product Name"><i class="fa fa-search"></i></a> </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    </section>

    <br /><br />
    
    <!-- Fun Fact -->
    <section class="fun-fact" data-stellar-background-ratio="0.5">
      <div class="container"> 
        
        <!-- Counter -->
        <div class="counters nolist-style">
          <ul class="row">
            
            <!-- Satisfied Clients -->
            <li class="col-sm-3"> 
              
              <!-- icon -->
              <div class="media-left"> <i class="fa fa-smile-o"> </i> </div>
              <div class="media-body"> <span class="counter" >1020</span>
                <p>Happy Customers</p>
              </div>
            </li>
            
            <!-- PROPOSALS SENT -->
            <li class="col-sm-3">
              <div class="media-left"> <i class="fa fa-users"> </i> </div>
              <div class="media-body"> <span class="counter">200</span>
                <p>Clients</p>
              </div>
            </li>
            
            <!-- AWARDS WON -->
            <li class="col-sm-3">
              <div class="media-left"> <i class="fa fa-folder-o"> </i> </div>
              <div class="media-body"> <span class="counter">1000</span>
                <p>Products</p>
              </div>
            </li>
            <li class="col-sm-3">
              <div class="media-left"> <i class="fa fa-ambulance"> </i> </div>
              <div class="media-body"> <span>24/7</span>
                <p>services available</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    
    <!-- News -->
    <section class="p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="heading-block">
          <h2>RECENT NEWS POSTS</h2>
          <hr>
          <span>Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie conseu vel illum dolore eufe ugiat nulla facilisis at vero.</span> </div>
        
        <!-- Latest News -->
        <div class="news">
          <div class="row"> 

            @if(count($news) == 0)
            
            <h5>No news yet!</h5>

            @else

              @foreach($news as $n)
              <div class="col-md-4"> 
                <article class="news-post">
                <div class="post-img"> <img class="img-responsive" src="{{ $n->cover_image }}" alt="">
                    <div class="date"> {{ date('d M', strtotime($n->created_date)) }} </div>
                  </div>
                  <a href="{{ route('news.show', [$n->slug]) }}" class="tittle">{{ Str::limit($n->title, 33) }}</a> <span class="by">By {!! $n->author->name !!}</span>
                  <p>
                    {{ strip_tags(Str::limit($n->body, 500)) }}
                  </p>
                  
                  <div class="post-info"> 
                    {{-- <span><i class="fa fa-comments-o margin-right-10"></i>97</span> 
                    <span><i class="fa fa-eye margin-right-10"></i>563</span>  --}}
                    <span><i class="fa fa-tag margin-right-10"></i>{{ $n->category['title'] }}</span> 
                    <a href="{{ route('news.show', [$n->slug]) }}">More <i class="fa fa-angle-right margin-left-10"></i></a> 
                  </div>
                </article>
              </div>
              @endforeach

            @endif

          </div>
        </div>
      </div>
    </section>
  </div>
@endsection