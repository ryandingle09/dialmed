<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_images extends Model
{
    public function category()
    {
        return $this->hasOne('App\Gallery', 'id', 'gallery');
    }
}
