<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    // private $title;

    // public function __construct(\App\News_tag $title) 
    // {
    //     $this->title = $title;
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category   = $request->input('category');
        $tag        = $request->input('tag');
        $search     = $request->input('search');
        $news       = \App\News::with(['category', 'author']);

        if($category) $news = $news->where('caegory_id', $category);
        if($tag) $news = \App\News_tag::where('tag_id', $tag)->with('news');
        if($search) $news = $news->where('title', 'like', '%'.$search.'%');

        $data = [
            'news' => $news->orderBy('id', 'DESC')->paginate(15)
        ];

        return view('news.news', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = [
            'categories'    => \App\Category::orderBy('id', 'DESC')->get(),
            'tags'          => \App\Tag::orderBy('id', 'DESC')->get(),
            'recent_news'   => \App\News::where('slug', '!=', $slug)->take(6)->orderBy('id', 'DESC')->get(),
            'news'          => \App\News::with(['category', 'author'])->where('slug', $slug)->get()[0]
        ];

        return view('news.single', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
