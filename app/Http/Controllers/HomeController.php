<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'news' => \App\News::with(['category', 'author'])->take(6)->get()
        ];

        return view('home.home', $data);
    }
}
