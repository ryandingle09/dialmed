<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News_tag extends Model
{
    public function news() 
    {
        return $this->belongsTo('App\News')->with(['author', 'category']);//->where('title', 'like', '%'.$this->title.'%');
    }
}
