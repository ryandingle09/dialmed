<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['web'])->group(function () {

    // Auth::routes(['verify' => true]);
    // Auth::routes();

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/news', 'NewsController@index')->name('news');
    Route::get('/news/{url}', 'NewsController@show')->name('news.show');
    Route::get('/news-by-category', 'NewsController@index')->name('news.category.list');
    Route::get('/news-by-tag', 'NewsController@index')->name('news.tag.list');
    
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/environmental', 'EnvironmentalController@index')->name('environmental');
    Route::get('/products', 'ProductsController@index')->name('products');
    Route::get('/equipment', 'EquipmentController@index')->name('equipment');
    Route::get('/contact', 'ContactController@index')->name('contact');
    Route::get('/gallery', 'GalleryController@index')->name('gallery');

    // excess
    // Route::get('/services', 'ServicesController@index')->name('services');
    
});

Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    
});
